from django import forms
from phonenumber_field.formfields import PhoneNumberField


class EntryForm(forms.Form):
    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Введите Вашу фамилию'}),
        label="Фамилия", min_length=3, max_length=20)
    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Введите Ваше имя'}),
        label="Имя", min_length=3, max_length=20)
    middle_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Введите Ваше отчество'}),
        label="Отчество", min_length=3, max_length=20)
    phone_number = PhoneNumberField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Оставьте Ваш телефон, начиная с +'}),
        label="Номер телефона")
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'Оставьте ваш email'}),
        label="Электронная почта")
    info = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'form-control col-xs-3', 'placeholder': 'Оставьте пару слов о себе'}),
        label="Информация", max_length=1024)

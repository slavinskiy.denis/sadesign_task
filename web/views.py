from django.http import HttpResponseRedirect
from django.shortcuts import render
from phonenumbers import phonenumberutil

import crm
from sadesign_task.settings import MEGAPLAN_SETTINGS as mp_settings
from web.forms import EntryForm


# Create your views here.
def get_form(request):
    if request.method == "POST":
        form = EntryForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data["first_name"]
            middle_name = form.cleaned_data['middle_name']
            last_name = form.cleaned_data['last_name']
            phone_number = str(phonenumberutil.format_number(
                form.cleaned_data["phone_number"],
                phonenumberutil.PhoneNumberFormat.E164))
            email = form.cleaned_data["email"]
            info = form.cleaned_data["info"]
            try:
                link = crm.MegaplanService(mp_settings['crm'], mp_settings['login'], mp_settings['password'])
                user_id = link.add_contractor(first_name, last_name, middle_name, email, phone_number, info)
                link.add_deal(user_id, 2, 'Сделка века')
            except (crm.MegaplanAuthorizationException, crm.MegaplanRequestException):
                return HttpResponseRedirect("/error")
            return HttpResponseRedirect("/ok")
    else:
        form = EntryForm()
    return render(request, "entry_form.html", {"form": form})


def get_ok(request):
    return render(request, "ok.html")


def get_error(request):
    return render(request, "error.html")

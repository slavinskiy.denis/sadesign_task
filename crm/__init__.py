# -*-coding:utf8-*-
import base64
import hashlib
import hmac
from datetime import datetime
from email import utils

import requests

HUMAN_VALUE = 'human'
PHONE_NUMBER_TYPE_MOBILE_VALUE = 'ph_m'

MODEL_FIRST_NAME_IN = 'Model[FirstName]'
MODEL_MIDDLE_NAME_IN = 'Model[MiddleName]'
MODEL_LAST_NAME_IN = 'Model[LastName]'
MODEL_TYPE_PERSON_IN = 'Model[TypePerson]'
MODEL_EMAIL_IN = 'Model[Email]'
MODEL_PHONES_IN = 'Model[Phones]'
MODEL_DESCRIPTION_IN = 'Model[Description]'
MODEL_CONTRACTOR_IN = 'Model[Contractor]'
PROGRAM_ID_IN = 'ProgramId'
DESCRIPTION_IN = 'Description'

AUTHORIZATION_URI = "/BumsCommonApiV01/User/authorize.api"
POST_METHOD = 'POST'
GET_METHOD = 'GET'


class MegaplanAuthorizationException(Exception):
    pass


class MegaplanRequestException(Exception):
    pass


class MegaplanService:
    def __init__(self, crm, login, password, protocol="https://"):
        """ Initializes Megaplan API helper and authorizes in crm system
        :param crm: uri crm address without protocol
        :param login: user login for access to Megaplan crm
        :param password: password: password for access to Megaplan crm
        """
        self.crm = crm
        self.protocol = protocol
        self.access_id = None
        self.secret_key = None
        self.user_id = None
        self.employee_id = None
        self.content_type = ''
        self.authorize(login, password)

    def authorize(self, login, password):
        """ Authorizes in the crm
        https://dev.megaplan.ru/1611/api/API_authorization.html
        :param login: user login for access to Megaplan crm
        :param password: password for access to Megaplan crm
        :return: True if authorization has been success
        """
        params = {'Login': login,
                  'Password': hashlib.md5(password.encode('utf-8')).hexdigest()}
        data = self.call(AUTHORIZATION_URI, method=POST_METHOD, params=params)
        self.access_id = data.get("AccessId")
        self.secret_key = data.get("SecretKey")
        self.user_id = data.get("UserId")
        self.employee_id = data.get('EmployeeId')
        if self.access_id is None or self.secret_key is None:
            raise MegaplanAuthorizationException("access_id or secret_key not exists")
        return True

    def sign(self, cmd, method, content_type, now):
        """ Makes signature for the request to Megaplan API
        https://dev.megaplan.ru/1611/api/API_authorization.html#id13
        :param cmd: Megaplan api link
        :param method: get or post
        :param content_type: content_type for the request
        :param now: current time im rfc2822 format
        :return: encrypted sign string for current request
        """
        uri = '{0}{1}'.format(self.crm, cmd)
        text = '\n'.join([method, '', content_type, now, uri]).encode()
        key = self.secret_key.encode()
        mac = hmac.new(key, text, hashlib.sha1)
        return base64.b64encode(mac.hexdigest().encode()).decode()

    def call(self, cmd, method=GET_METHOD, params={}):
        """ Makes call to the Megaplan API
        https://dev.megaplan.ru/1611/api/common.html
        :param cmd: Megaplan api link
        :param method: Http-method for the request. Can be 'GET' or 'POST
        :param params: Megaplan params for the request
        :rtype: dict
        :raise: MegaplanRequestException
        :return: method returns dict that contains all fields from
         [data] part of json's Megaplan answer
        """
        now_rfc2822 = utils.formatdate(datetime.now().timestamp())
        uri = '{0}{1}'.format(self.crm, cmd)
        headers = {'Date': now_rfc2822, 'Accept': 'application/json'}
        if method.upper() == POST_METHOD:
            content_type = 'application/x-www-form-urlencoded'
            headers.setdefault('Content-Type', content_type)
        else:
            content_type = self.content_type
        if cmd != AUTHORIZATION_URI:
            headers.setdefault('X-Authorization', '{0}:{1}'.format(
                self.access_id, self.sign(cmd, method, content_type, now_rfc2822)))
        data = params
        request = requests.request(method, '{0}{1}'.format(self.protocol, uri), data=data, headers=headers)
        if request.status_code == 200:
            json = request.json()
            if json['status']['code'] == 'ok':
                return json['data']
            else:
                raise MegaplanRequestException('An error in the request:\n{0}'.format(str(json)))
        else:
            raise MegaplanRequestException(
                'Http-error during the request: {0}\nReason: {1}'.format(request.status_code, request.reason)
            )

    def add_contractor(self, f_name, l_name, m_name, email, phone, info):
        """ Adds new contractor to the crm
        https://dev.megaplan.ru/1611/api/API_contractors.html#id8
        :param f_name: first name of the new contractor
        :param l_name: last name of the new contractor
        :param m_name: middle name of the new contractor
        :param email: email address of the new contractor
        :param phone: phone number of the new contractor
        :param info: information of the new contractor
        :return: id of the new contractor
        """
        cmd = '/BumsCrmApiV01/Contractor/save.api'
        params = {
            MODEL_FIRST_NAME_IN: f_name,
            MODEL_MIDDLE_NAME_IN: m_name,
            MODEL_LAST_NAME_IN: l_name,
            MODEL_TYPE_PERSON_IN: HUMAN_VALUE,
            MODEL_EMAIL_IN: email,
            MODEL_PHONES_IN + '[0]': [PHONE_NUMBER_TYPE_MOBILE_VALUE
                                      + '-'.join((phone[0:len(phone) - 10],
                                                  phone[len(phone)-10:len(phone)-7],
                                                  phone[len(phone)-7:]))[::] + '\t'],
            MODEL_DESCRIPTION_IN: info
        }
        data = self.call(cmd, method=POST_METHOD, params=params)
        return data['contractor']['Id']

    def get_contractor_card(self, contractor_id):
        """ Returns contractor card as a dict
        https://dev.megaplan.ru/1611/api/API_contractors.html
        :param contractor_id: id of the contractor in the crm
        :return: dict that contains fields of contractor card in crm
        """
        uri = '/BumsCrmApiV01/Contractor/card.api'
        params = {'Id': contractor_id}
        return self.call(uri, method=POST_METHOD, params=params)

    def add_deal(self, contractor_id, program_id, description):
        """ Adds new deal to the crm
        https://dev.megaplan.ru/1611/api/API_deals.html
        :param contractor_id: id of the contractor in the crm
        :param program_id: id of the deal program in crm
        :param description: additional information of the dial
        :return: id of the dded deal
        """
        uri = '/BumsTradeApiV01/Deal/save.api'
        params = {
            MODEL_CONTRACTOR_IN: contractor_id,
            PROGRAM_ID_IN: program_id,
            DESCRIPTION_IN: description
        }
        data = self.call(uri, method=POST_METHOD, params=params)
        return data['deal']['Id']

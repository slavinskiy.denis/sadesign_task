import datetime
import json
import re
from enum import Enum, unique

import requests

AUTHORIZATION_URL = '/private/api/auth.php'

GET_METHOD = 'GET'
POST_METHOD = 'POST'


@unique
class FieldType(Enum):
    text = 1
    numeric = 2
    checkbox = 3
    select = 4
    multi_select = 5
    date = 6
    url = 7
    multi_text = 8
    textarea = 9


@unique
class ElementType(Enum):
    contact = 1
    lead = 2
    company = 3


class AmoCrmAuthorizationException(Exception):
    pass


class AmoCrmServiceException(Exception):
    pass


class AmoCrmService:
    def __init__(self, crm, login, user_hash):
        """
        Initializes the AmoCRM api service and authorizes in the AmoCRM
        :param crm: a url of AmoCRM account without the protocol
        :param login: an email that uses as login in the AmoCRM
        :param user_hash: a secret hash from settings of the AmoCRM account
        """
        crm = re.sub(r'http(s)?://', '', crm)
        self.crm = crm
        self.protocol = 'https://'
        self.session = None
        self.authorize(login, user_hash)
        self.account_info = self.get_account_info()
        self._set_custom_fields()
        self._set_leads_statuses()

    def authorize(self, login, user_hash):
        """https://developers.amocrm.ru/rest_api/#auth
        authorizes in the AmoCRM
        :param login:
        :param user_hash:
        :return: None
        :raise AmoCrmAuthorizationException if couldn't access to the crm with the given params
        """
        params = {'USER_LOGIN': login,'USER_HASH': user_hash}
        if not self.call(AUTHORIZATION_URL, POST_METHOD, params)['auth']:
            raise AmoCrmAuthorizationException()

    def call(self, cmd, method=GET_METHOD, params={}):
        """https://developers.amocrm.ru/introduction.php
        Makes a request to the AmoCRM API
        :param cmd: path to the api without domain
        :param method: get or post required
        :param params: dictionary that contains all required fields of the request
        :return: the response part of the json answer from the server
        :raise AmoCrmAuthorizationException
        """
        if self.session is None:
            self.session = requests.Session()
        headers = {'Accept': 'application/json'}
        url = "{0}{1}{2}?type=json".format(self.protocol, self.crm, cmd)
        if cmd != AUTHORIZATION_URL:
            params = {'request': params}
        request = self.session.request(method, url, data=json.dumps(params), headers=headers)
        if request.status_code == 204:
            return
        if request.status_code == 200:
            return request.json()['response']
        else:
            raise AmoCrmAuthorizationException(
                'Http-error during the request: {0}\nReason: {1}\nDetails: {2}'.format(
                    request.status_code, request.reason, request.content.decode())
            )

    def _set_custom_fields(self):
        """https://developers.amocrm.ru/rest_api/fields_info.php
        makes a dictionary with custom fields info
        :return: None
        :raise AmoCrmServiceException
        """
        if self.account_info is None:
            raise AmoCrmServiceException('No account info information')
        custom_fields = {}
        for field in self.account_info['custom_fields']['contacts']:
            custom_fields[field['name']] = {'id':  field['id']}
            if 'enums' in field:
                custom_fields[field['name']]['enums'] = {value: key for key, value in field['enums'].items()}
        self.custom_fields = custom_fields

    def _set_leads_statuses(self):
        """https://developers.amocrm.ru/rest_api/leads_set.php
        makes a dictionary with statuses of the leads from account info
        :raise AmoCrmServiceException
        """
        if self.account_info is None:
            raise AmoCrmServiceException('No account info information')
        self.leads_statuses = {entry['name']: entry['id'] for entry in self.account_info['leads_statuses']}

    def _make_custom_field(self, name, value, enum=''):
        """https://developers.amocrm.ru/rest_api/fields_set.php
        Makes a custom field dictionary in the Amo CRM request format
        if custom field is not set to the current account adds it for them
        :param name: custom field name
        :param value: custom field value
        :param enum: value of the enum if custom field requires it
        :return: dict with custom field data
        """
        if name not in self.custom_fields:
            self.add_custom_field(name, field_type=FieldType.text)
            self.account_info = self.get_account_info()
            self._set_custom_fields()
        field = {
            'id': self.custom_fields[name]['id'],
            'values': [{
                'value': value
        }]}
        if enum != '':
            field['values'][0]['enum'] = enum
        return field

    def add_contact(self, name, email, phone, info):
        """https://developers.amocrm.ru/rest_api/contacts_set.php
        Makes request to add a new contact to the Amo CRM
        :param name: full name
        :param email: contact's email that marks private
        :param phone: contact's phone that sets up mobile
        :param info: additional information of this contacts
        :return: id of added contact
        """
        cmd = '/private/api/v2/json/contacts/set'
        params = {'contacts': {'add': [{
            'name': name,
            'custom_fields': [
                self._make_custom_field('Email', email, 'PRIV'),
                self._make_custom_field('Телефон', phone, 'MOB'),
                self._make_custom_field('Доп. информация', info)
            ]
        }]}}
        return self.call(cmd, method=POST_METHOD, params=params)['contacts']['add'][0]['id']

    def add_lead_to_contact(self, lead_id, contact_id):
        """https://developers.amocrm.ru/rest_api/contacts_set.php
        Requests the current list of leads that linked to the contact and
        add the lead to it
        :param lead_id:
        :param contact_id:
        :return: True if lead has been successfully added or either returns False
        """
        cmd = '/private/api/v2/json/contacts/set'
        info = self.get_contact_info(contact_id)
        leads = info['linked_leads_id']
        leads.append(lead_id)
        params = {'contacts': {'update': [{
            'id': contact_id,
            'linked_leads_id': leads,
            # fixme: can't update contact with system time. Error that date is older than in DB
            'last_modified': int(datetime.datetime.now().timestamp()) + 100
        }]}}
        if 'errors' in self.call(cmd, POST_METHOD, params)['contacts']['update']:
            return False
        return True

    def add_custom_field(self, name, field_type=FieldType.text,
                         element_type=ElementType.contact):
        """"https://developers.amocrm.ru/rest_api/fields_set.php
        Adds a custom field to the AmoCtm account
        :param name:
        :param field_type: must be one of the FieldType values.
        Determines to which type will be the custom field
        :param element_type: must be one of the ElementType values.
        Determines to which entities will apply the creating custom field
        :return: id of the created custom field
        """
        cmd = '/private/api/v2/json/fields/set'
        params = {'fields': {'add': [{
            'name': name,
            'disabled': 0,
            'type': field_type.value,
            'element_type': element_type.value,
            'origin': 'amocrm_python_client'
        }]}}
        return self.call(cmd, POST_METHOD, params)['fields']['add'][0]['id']

    def add_lead(self, name, price, status_id="Первичный контакт"):
        """
        Adds a new lead
        :param name: name of the deal
        :param price: budget of the deal
        :param status_id: status of the deal
        :return: id of the created deal
        """
        cmd = '/private/api/v2/json/leads/set'
        params = {'leads': {'add': [{
            'name': name,
            'status_id': status_id,
            'price': price
        }]}}
        return self.call(cmd, POST_METHOD, params)['leads']['add'][0]['id']

    def get_account_info(self):
        """https://developers.amocrm.ru/rest_api/accounts_current.php
        requests for the account data and return it as a dict
        :return:
        """
        cmd = '/private/api/v2/json/accounts/current'
        return self.call(cmd)['account']

    def get_contact_info(self, contact_id):
        """https://developers.amocrm.ru/rest_api/contacts_list.php
        requests for the contact data and return it as a dict
        :param contact_id: id of the current contact
        :return: dict with the contact data
        """
        cmd = '/private/api/v2/json/contacts/list'
        params = {'contacts': [{
            'id': contact_id
        }]}
        return self.call(cmd, params=params)['contacts'][0]
